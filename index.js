/* 

http methods

http methos of the incoming request can be accessed via the
method property of the request parameter

get- method for a request that indicates that we want to retrieve data


post - method for a request that indicates that we want to
     add or send data to the server for processing. it is 
     usually used for creating new entries 


delete - method to delete a specified doc


*/


const http = require('http');

// cost port = 4000;

http.createServer( function (request, response ) {

	if (request.url === "/" && request.method === "GET") {


		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("new server")
	}
}).listen(4000)

console.log("server is running")
